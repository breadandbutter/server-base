include_directories(
    ${CMAKE_SOURCE_DIR}/src/framework
    ${CMAKE_SOURCE_DIR}/src/bindings/universal
    ${CMAKE_SOURCE_DIR}/src/shared
    ${OPENSSL_INCLUDE_DIR}
    ${ACE_INCLUDE_DIR}
)

add_executable(bignum-test
    test.cpp
    ${CMAKE_SOURCE_DIR}/src/shared/Auth/BigNumber.cpp
    ${CMAKE_SOURCE_DIR}/src/shared/Auth/BigNumber.h
    ${CMAKE_SOURCE_DIR}/src/shared/Common.cpp
    ${CMAKE_SOURCE_DIR}/src/shared/Common.h
)

target_link_libraries(bignum-test
    ${OPENSSL_LIBRARIES}
)

add_test(bignum-test bignum-test)
